'use strict'

// DOM Document object model

//Con esto tomo el elemento html con este div y si quiero el texto que tiene dentro uso .innerHTML;
//var valor = document.getElementById("micaja").innerHTML; 

//var valor2 = document.getElementById("micaja").innerHTML; 
//document.write("<p>Tu Etiqueta contenia : "+valor2+"</p>");

/* 1 Conseguir elementos con un ID concreto */
function camcolor (deseado){
    valor.style.background = deseado;    
}

//Existen varias formas de tomar elementos html mediante js
//var valor = document.getElementById("micaja"); //Tomamos el elemento html por medio del id="micaja"

//Usando el querySelector es buenisimo dentro d los parentesis colocamos clases .clase por medio de id #elemento
// o colocando la etiqueta htmldirecatamente.
var valor = document.querySelector("#micaja"); // es mejor usar estamas sencilla

valor.innerHTML= "Texto insertado desde Js"; //a dicho elemento le agregamos un nuevo texto

//Tambien podemos agregar stylos desde el js
valor.style.padding = "20px"; //Le agregamos estilos
valor.style.color = "black";

//Tambien podemos agregarle clases los elementos html por medio de js.
valor.className ="saltar";

//llamamos a la funcion yle pasamos por parametro un color
//camcolor("blue");


/*2 Conseguir elemento por su etiqueta */
//Por medio de esto obtengo en un array de todos los div del documento
var todosLosdiv = document.getElementsByTagName('div');
console.log(todosLosdiv);




//accedo al div y extraigo lo que desee
//var contenidoDiv = todosLosdiv[2].textContent; 
/*var elemento = todosLosdiv[2]; //Accedo a cualquier div mediante su posicion en el array
elemento.innerHTML = "Hola Mundo (texto insertado desde Js)";//Inserto un nuevo texto
elemento.style.background= "blue";//Inserto un nuevo color  
*/


//todosLosdiv.forEach((valor,indice)=>{

    var valor;
for(valor in todosLosdiv){
    console.log(todosLosdiv[valor]);
    if(typeof todosLosdiv[valor].textContent== 'string'){
        var parrafo = document.createElement("p"); //creo un nodo de elemento p
        var texto = document.createTextNode(valor); //creo un nodo de texto
        parrafo.appendChild(texto); //al elemento (p) insertale el (texto)
        document.querySelector("#miseccion").appendChild(parrafo);//insertalo en en el div #miseccion
    }
    
}

/*3 Conseguir un elemento por su clase */
var div_azul = document.getElementsByClassName('azul'); 

//div_rojo[0].style.background="blue"; //asi le aplcaria azul a un solo div

//s queremos aplicar estilos a varios como es unacadena usamos un for
var recorre;//variable con laq voy a recorrer todos los div con clase azul
for(recorre in div_azul){
    //verifico q el elemento de la cadena div_azul tiene una clase igual azul
    if(div_azul[recorre].className == "azul"){
    div_azul[recorre].style.background = "blue"; //le aplicamos el color azul

}


}
//});

//Query Selector es q nos seleccion una clase o un id sin ningun problema
//pero para seleccionar muchas clases y etiquetas no me sirve
//clase
var id = document.querySelector(".encabezado");
console.log(id);
//id




