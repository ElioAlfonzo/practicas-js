'use strict'

//Condicionales
// es un estructura de control mediante distintas condiciones

var edad1 = 25;
var edad2 = 28;
var nombre = "Elio Alfonzo";
// Operadores condicionales
/*Mayor >
Menor <
Mayor o igual >=
Menor o igual <=
igual ==
Diferente !=
*/

// if (edad1>edad2){
// console.log("La edad 1 es mayor");
// }else{
//     console.log("La edad 2 es mayor");
// }

if (edad1 >=18){
    console.log("Es mayor de edad");

    if(edad1>30){
        console.log("Con una edad mayor a los 20 años");
    }else if(edad1<30){
        console.log("Eres mayor pero con una edad menor a los 30 años");
    }else{
        console.log("no tienes edad");
    }
}else{
    console.log("Es menor de edad:"+edad1);
}


// Operadores Logicos
/* AND (Y): &&
   OR (O): ||
   Negacion: ! 
*/
var year = 2028;
//Negacion 
if(year != 2016){
    console.log("El año no es 2016");
}

//AND
if(year >= 2000 && year<=2020){
    console.log("Estamos en la era actual");
}else{
    console.log("Estamos en la epoca post-moderna");
}

// OR  Podemos darle prioridad a cual evaluacion tomar primero

if(year==2008 || (year > 2018 && year >= 2028) ){
    console.log("El Año termina en 8");
}


//











