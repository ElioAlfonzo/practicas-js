'use strict'
//Json-Placeholder
//Es Un Api-Rest es un servicio o backend que recibe datos en json, puede recibir peticiones http: get,post,delete, 
//Fetch(ajax) es un sustituto o un metodo que hace peticiones ajax (asincronas) a un servidor/ peticiones y servicios /api rest
//Vamos a consumir un servicio rest
//esto es una promesa que asu vez contiene metodo como por ejemplo .then()
var espacio_html = document.querySelector('#usuarios');
var usuarios = [];
fetch('https://jsonplaceholder.typicode.com/users') //accceder a un servicio remoto y hace la peticion
    .then(data => data.json()) //con then recogemos datos y lo guardamos en data, lo formateamos a json con data.json
    .then(data => {
        usuarios = data;
        console.log(usuarios);
    });


//Peticiones AJAX
//Usando la api rest de otra pagina https://reqres.in/
var usuarios2 = [];

fetch('https://reqres.in/api/users')
    .then(data1 => data1.json()) //primero guardamos en data1 los datos convertidos en formato json
    .then(usuarios => { //users informacion a capturada en tipo json
        usuarios2 = usuarios.data; //guardamos data1 en la cadena que creamos usuarios2 = usuarios.data(especificamos los datos q queremos)
        console.log(usuarios2);

        listar_usuarios();
    });


function listar_usuarios(){
    usuarios2.map((usuario, i) => {
        //i += 1;
        let nombre = document.createElement('h3');

        nombre.innerHTML = "Posi: " + i + " " + usuario.first_name + " " + usuario.last_name;
        espacio_html.appendChild(nombre);

        document.querySelector(".loading").style.display = 'none'; //despues que se cargen me oculte la etiqueta span(cargando)
    });
}