'use strict'

//cuando cargue toda a ventana

window.addEventListener('load', function () {
    console.log("Hola Mundo");
    var formu = document.querySelector("#formulario");//asi seleccionamos un id en el html
    var box_dashed = document.querySelector(".dashed"); //asi seleccionamos una clase en html
    box_dashed.style.display = "none"; //no visible 



    formu.addEventListener('submit', function () { //Estoy trabajando todo dentro del formulario
        console.log("Submit Capturado");

        //Un dato importante, todos los datos introducidos en un input son caracteres hasta q yo los convierta en enteros
        //con value le saco el valor que tiene dentro
        var nombre = document.querySelector("#nombre").value;
        var apellido = document.querySelector("#apellido").value;
        var edad = parseInt(document.querySelector("#edad").value);
        console.log(typeof (edad));

        //Tomamos las variables y Validamos los campos que no esten vacios ni con datos incorrectos con js puro
        //trim() eliminamos los espacios por delante  por detras
        if (nombre.trim() == null || nombre.trim().length == 0) {
            document.querySelector("#error_nombre").innerHTML="El nombre no es valido";
            alert("el nombre no es valido");
            return false; //el uso del reutrn false es para que no se siga ejecutandose
        }else{
            document.querySelector("#error_nombre").style.display="none";
        }
        if (apellido.trim() == null || apellido.trim().length == 0) {
            alert("el apellido no es valido");
            return false;
        }
        if (edad == null || edad <= 0 || isNaN(edad)) {
            alert("la edad no es valido");
            return false;
        }
        //ahora cuando yo haga submit me lanze un display es decir me lo muestre (visible)
        box_dashed.style.display = "block";

        console.log(nombre + " " + apellido + " " + edad);
        var datos_usuario = [nombre, apellido, edad];

        /*1era forma */
        /*
        var indice = 0;
        for(indice in datos_usuario){
            var parrafo = document.createElement("p"); //creamos la etiqueta p
            parrafo.append(datos_usuario[indice]); //agrega a las etiqueta p la primera informacion del array
            box_dashed.append(parrafo); //ahora agrega la informacion dentro de la el id .dashed
        }*/

        /*2da forma */
        /*Sleccionamos el id de los campos del dashed */
        var p_nombre = document.querySelector("#p_nombre span");
        var p_apellido = document.querySelector("#p_apellido span");
        var p_edad = document.querySelector("#p_edad span");

        p_nombre.innerHTML = nombre;
        p_apellido.innerHTML = apellido;
        p_edad.innerHTML = edad;
    });

});



;
