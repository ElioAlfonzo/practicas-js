'use strict'

window.addEventListener('load', function () {

    //Timers
    //setInterval: ejecuta cada cierto intervalo de tiempo
    //Ejecutate una sola vez en el tiempo que te especificamos

    function intervalo() {
        var tiempo = setInterval(function () {
            console.log("Set Interval");

            var texto = document.querySelector("h1");
            console.log(texto);
            if (texto.style.fontSize == "50px") {
                texto.style.fontSize = "30px";
            } else {
                texto.style.fontSize = "50px";
            }ºº
º
        }, 1000);
        return tiempo;
    }

    var tiempo = intervalo();

    var detener = document.querySelector("#stop");
    var inicar = document.querySelector("#iniciar");

    detener.addEventListener("click", function () { //funcion que detiene el tiempo
        alert("Has detenido el bucle");
        clearInterval(tiempo); //Detenemos e bucle
    });


    iniciar.addEventListener("click", function () { //funcion que detiene el tiempo
        console.log("Has iniciado nuevamente el reloj")
        intervalo(); //Detenemos e bucle
    });
});