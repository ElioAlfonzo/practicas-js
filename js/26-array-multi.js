'use strict'

//Los array multidimensionales son arrays dentro de otros arrays
//Dentro del array podemos meter de todo booleano, enteros, string, etc.
var categorias = ['Accion', 'Comedia', 'Terror'];
var peliculas = ['La verdad duele','La vida es bella', 'Gran torino' ];
var perro = ['perro1','perro2'];
var cine = [categorias,peliculas,perro];

console.log(cine[0][2]);

console.log(cine[2][1]);