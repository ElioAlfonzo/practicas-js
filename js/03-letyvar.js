'use strict'
// La Principal diferen entre el let y var es el alcanze que tienen cada una de ellas
//let se usa ahora en js normal, 

//Diferencias entre definir variables con de let y var    
//let: nos pemite declarar variables limitando su alcanze al bloque de declaracion o expresion donde se usa
//var: define variables globales o locales en una funcion sin importar el ambito del bloque   

//En conclusion con let limitamos, es una variable con alcance limitado dentro d un bloque o seccion donde este
//var se puede ir actualizando en distintas partes de mi codigo

var numero = 40;
console.log(numero);
if(true){
    var numero = 50;
    console.log(numero);
}

console.log(numero);

var texto = "Curso js www.victorroblesweb.com";
console.log(texto);

if(true){
    // let lo q hace es actuar a nivel de bloque es decir la variable con let solo tiene vida dentro d este bloque
    let texto = "Curso de laravel 5 Elio.Alfonzo";
    console.log(texto); //Laravel
}
//Aqui se imprime lo que tiene la variable con var
console.log(texto);



