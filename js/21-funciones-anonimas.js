'use strict'

//Funciones anonimas
//Es una funcion que no tiene nombre es decr que ese conjunto de intrucciones no tiene nombre y lo podemos
//guardar dentro de una variable

//Callback es una funcion que se ejecuta dentro de otra

/*
var pelicula = function(nombre){
    return "La pelicula es "+nombre;
}
*/

//sumaymuestra recibe es una accion la cuales la funcion que le estoy enviando
function sumame(num1, num2, sumaymuestra, sumapordos) {
    var suma = num1 + num2;
    sumaymuestra(suma);

    var texto = "El resultado es " + suma;

    sumapordos(texto);
    return suma;
}


sumame(5, 8, function (dato) {
    console.log("El resultado de la suma: ", dato);

},
    function (dato1) {
        console.log(dato1);
    }
);


