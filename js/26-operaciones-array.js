'use strict'

var categorias = ['Deporte','Emprendimiento','Amor'];
var peliculas = ['El tiempo', 'Nada es facil', 'De vuelta por ti'];

//Ordenar un array
console.log(peliculas.sort());//Por defecto lo ordena por orden alfabetico 
var informacion = [categorias,peliculas];

/*
var dato = prompt("inserta un nuevo elemento");

while(dato!='Acabar'){
    peliculas.push(dato);// Push para agregar nuevos elementos al array
    var dato = prompt("inserta un nuevo elemento");   
}

//peliculas[0]=undefined;//para meter un undefined en esta posicion

var size= peliculas.length;//Tamaño del array
var posicion_elimi= size-2;

//peliculas.pop(); //pop() me elimina el ultimo elemento del array
console.log(posicion_elimi);

peliculas.splice(posicion_elimi,1);


console.log(peliculas);

var convert_string =peliculas.join();
console.log(convert_string);
*/

var textos = "Casa,auto,familia";

// "" lo separa por letra
// " " separa por palabra
//  , separa por espacio
var convert_array = textos.split(" "); 

console.log(convert_array);


