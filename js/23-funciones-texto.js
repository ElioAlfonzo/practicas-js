'use strict'

var numero = 44;
var texto = "Bienvenida al curso de Javascript";
var texto2 = "Es muy bueno";

//  Para Transformar Texto. //

var cambio=numero.toString();//Convertir string
console.log(typeof cambio);//Ver que tipo de datos es
console.log(texto2.toUpperCase());//Mayuscula   
console.log(texto2.toLowerCase());//Minuscula

// Para Calcular longitud //

var nombre =  "Elio" ;
var nombre2 = ['Elio','Alfonzo'];

console.log(nombre.length);//Mediomos lacantidad de caracteres
console.log(nombre2.length);//Medimos la cantidad de elementos en el array

//Concatenar texto
 
var textojunto = texto + texto2;
var textojunto2 = texto.concat(texto2);

console.log(textojunto);
console.log(textojunto2);

//Metodos de busquedas

var busqueda = texto.indexOf("al"); //en que posicion esta al 
console.log(busqueda);

var busqueda2 = texto.match("de"); //nos dice en que posicion del texto se encuentra de
console.log(busqueda2);

var busqueda3 = texto.substr(15,5);//nos sustrae texto de la posicion 15 extraeme 5 caracteres
console.log(busqueda3);

var busqueda4 = texto.charAt(3); //toma el caracter ubicado en la posicion 3
console.log(busqueda4);

var busqueda5 = texto.startsWith("B"); //Busca al inicio del string a ver si consigue esa plabra q le pasamos
console.log(busqueda5);

var busqueda6 = texto.endsWith("B"); //Busca al Final del string a ver si consigue esa plabra q le pasamos
console.log(busqueda6);

var busqueda7 = texto.includes("curso"); //Busca en el texto esta palabra e imprime true o false
console.log(busqueda7);







