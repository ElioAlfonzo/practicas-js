'use strict'

//Funciones
//Es una agrupacion de un conjunto de instrucciones reutilizables

function consola(num1,num2){
    console.log("***********Consola*****************");
    console.log("Suma:"+(num1+num2));
    console.log("Resta:"+(num1-num2));
    console.log("Multiplicacion:"+(num1*num2));
    console.log("Division:"+(num1/num2));
    
}

function cuerpo(num1,num2){
    document.write("***********Cuerpo*****************"+"</br>");
    document.write("Suma:"+(num1+num2)+"</br>");
    document.write("Resta:"+(num1-num2)+"</br>");
    document.write("Multiplicacion:"+(num1*num2)+"</br>");
    document.write("Division:"+(num1/num2)+"</br>");
}


function calculadora(num1,num2,mostrar=false){

    if(mostrar == false){   
        consola(num1,num2);
    
}else{
    cuerpo(num1,num2);
}
}

//Invocamos nuestra funcon calculadora
calculadora(6,7,false); //Me lo muestra por consola
calculadora(6,7,true); //Me lo muestra por el cuerpo html
/* 
for(var i=1; i<=10; i++){
    console.log()
    calculadora(i,8);
}
*/
