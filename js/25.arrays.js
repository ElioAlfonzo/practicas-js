'use strict'

//Array Son variables que almacenan una colecccion de datos
//En estos array puedo meter lo q quiera: funciones, variables, cadenade texto, ,num,booleanos,etc

//Ejemplo
var nombre = "Victor Robles";
var nombres = ["Elio","Victor","Jose marin",53,true]

//Tambien podemos hacer array en formade obj
var lenguajes = new Array("Php", "Js", "Css");

//console.log(nombres[3]);
//console.log(lenguajes.length);

/*
var num = parseInt(prompt("Que elemento desea",0));

if(num >nombres.length){
    alert("Por favor verifique su numero, menor a: "+nombres.length);

}else{
console.log(nombres[num]);
}
*/ 

/*Esta es la manera clasica de recorrer un array*/ 

/* 
document.write("<h1>Uso del foreach para recorrer un array</h1>");
document.write("<ul>");
for(var i = 0; i<lenguajes.length; i++){
    document.write("<li>"+lenguajes[i]+"</li>");
}
document.write("</ul>");
*/

//Usamos mejor es mas elegante y avazado el metodo Foreach | Y una funcion de callback o de flecha
//va a recibir 3 parametros elementos,index,array
/* 
document.write("<ul>");
nombres.forEach((elemento,index,arr)=>{
    console.log(arr);//con esto me trae todo el array
    document.write("<li>"+index+" "+elemento+"</li>");//elemento me trae cada valor del array | index trae la posicion
});
document.write("</ul>");
*/

//Otra forma de recorrer un array

for(let lenguaje in lenguajes){
    document.write("<li>"+lenguajes[lenguaje]+"</li>")
}

//Busqeda con condiciones en un array
var precios = [20, 30, 40];
var ver = precios.some(precio=>precio >20);

console.log(ver);