'use strict'

/*
Calculadora
1. Que pida 2 numeros por pantalla
2. Si metemos uno mal que lo vuelva a pedir
3. Mostrar en el body en una alert y por la consola el resultado de sumar, restar,multiplicar y dividir
*/

var numero1 = parseInt(prompt("Inserte el primer numero",1));
var numero2 = parseInt(prompt("Inserte el segundo numero",1));

while (isNaN(numero1) || isNaN(numero2) || numero1 < 0 || numero2 < 0) {
    numero1 = parseInt(prompt("Inserte el primer numero",1));
    numero2 = parseInt(prompt("Inserte el segundo numero",1));
}

var resultado = "La suma de numero 1 y el numero 2 es: "+ (numero1 + numero2) + "</br>"+ 
"La resta de numero 1 y el resta 2 es: " +(numero1 - numero2) + "</br>"+ 
"La multiplicacion de numero 1 y el numero 2 es: " +(numero1 * numero2)  + "</br>"+
"La division de numero 1 y el numero 2 es: " +(numero1 / numero2)  + "</br>";

var resultadoalert = "La suma de numero 1 y el numero 2 es: "+ (numero1 + numero2) + "\n"+ 
"La resta de numero 1 y el resta 2 es: " +(numero1 - numero2) + "\n"+ 
"La multiplicacion de numero 1 y el numero 2 es: " +(numero1 * numero2)  + "\n"+
"La division de numero 1 y el numero 2 es: " +(numero1 / numero2)  + "\n";

console.log(resultado);
document.write(resultado);
alert(resultadoalert);
