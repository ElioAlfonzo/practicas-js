'use strict'
//Eventos del Raton
// Tomamos el boton que deseamos aplicarle los eventos


//agregamos a windows un evento donde una vez cargado todos los elementos del dom comienzen a ejecutarse todas las demas funciones
window.addEventListener('load', () => { 

    function cambiarColor() {
        var bg = boton.style.background;
    
        if (bg == "green") {
            boton.style.background = "red";
        } else {
            boton.style.background = "green";
        }
        boton.style.padding = "10px"
        boton.style.border = "1px solid #ccc"
    
        return true;
    }
    
    var boton = document.querySelector("#boton")
    
    //Evento Click
    boton.addEventListener("click", function () {
        cambiarColor();
        //con this podemos decr que lo que lasacciones seran agregadas a los eleemntos dentro de este bloque
        this.style.border= "10px solid black";
    });
    
    //Evento Mouse Over
    boton.addEventListener('mouseover', function () {
        boton.style.background = "#54cfd8f7";
    });
    
    boton.addEventListener('mouseout', function () {
        boton.style.background = "black";
    });
    
    //Focus: cuando entramos a un campo
    var campo = document.querySelector("#campo_nombre");
    campo.addEventListener('focus', function () {
        console.log("Esta dentro de campos evaluado con el evento focus");
    });
    
    //Blur: cuando salims de un campo
    campo.addEventListener('blur', function () {
        console.log("Estas fuera del campo con el evento blur");
    });
    
    //Keydown : Nos captura la tecla que estamos pulsando
    campo.addEventListener('keydown', function (tecla) {
        console.log("[Keydown]: Estas pulsando la siguiente tecla: ", String.fromCharCode(tecla.keyCode)); //capturamos la tecla y la convertimos en caracter
    });
    
    //Keypress
    campo.addEventListener('keypress', function (tecla2) {
        console.log("[Keypress]: Pulsastes la siguiente tecla: ", String.fromCharCode(tecla2.keyCode)); //capturamos la tecla y la convertimos en caracter
    });
    
    //Keyup
    campo.addEventListener('keypress', function (tecla3) {
        console.log("[Keypress]: Soltaste la siguiente tecla: ", String.fromCharCode(tecla3.keyCode)); //capturamos la tecla y la convertimos en caracter
    });

});
