'use strict'

//JSON Javascript Object Notation - Javascript notacion de objetos
//Los objetos json o de js se definen asi {} y le vamos indicando dentro de ellas las propiedades que tendra la misma

//Primer Objeto
var pelicula = {
    titulo : 'Batman vs Superman II',
    year : 2017,
    pais: 'Estados Unidos',
    
};

//Segundo Objeto
//Tambien podemos hacer un array de peliculas
var peliculas = [
    {titulo:'yakichan', year:2018, pais:'Hong Kong'},
    pelicula
]

//Podemos tener lo que deseemos array dentro de objetos, objetos dentro de arrays

console.log(peliculas);

var seccion = document.querySelector("#informacion");

var indice;
for(indice in peliculas){
    var p = document.createElement("p"); //creo mi elemento html
    p.append(peliculas[indice].titulo +"-"+peliculas[indice].year ); //le agrego a mi elemento html la informacion de mi objeto
    seccion.append(p); //agrego mi nuevo elemento html a mi bloque seccion

}