'use strict'

// BOM Browser Object Model
//Este nos da muchas propiedades para Trabajar con el navegador

function Bom(){
    console.log("Altura de la Ventana: "+window.innerHeight);
    console.log("Anchura de la Ventana: "+window.innerWidth);
    console.log("Altura de la Ventana Screen: "+screen.height);
    console.log("Anchura de la Ventana Screen: "+window.innerWidth);
    
    // Funciontion location

    console.log(window.location.href);
}
Bom();

function redirijir(redi){
    window.location.href = redi;
}

function abrirventa(venta){
    window.open(venta,"","with=400,height");
}

