'use strict'


//Parametro REST y SPREAD

//Con el parametro REST  ...parametro se pueden recibir un numero indefinido de parametros y los pasa en un array

//Con SPREAD podemos pasar un listado de elemntos sin ningun prolema es lo mismo que Rest


function listadoFrutas(fruta1,fruta2,...resto_frutas){
    console.log("Fruta 1: ", fruta1);
    console.log("Fruta 2: ", fruta2);
    console.log("Frutas Restantes: ", resto_frutas)  
    
}

//REST
listadoFrutas("Naranja", "Manzana","Parchita","Guanabana","Mango");

//SPREAD ES COMO LO INVERSO A REST
var frutas = ["Pera","Durazno"];
listadoFrutas(...frutas,"Parchita","Guanabana","Mango");

//var frutas = ["Melon","Pera"]; //Array
//listadoFrutas(...frutas,"Sandia","Pera","Melon");