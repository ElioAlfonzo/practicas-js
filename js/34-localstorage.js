'use strict'

//Localstorage nos sirve para almacenar informacion mientras estamos navegando //Parecido a las sessiones pero en el navegador


//Comprobar disponibilidad del localstorage
if (typeof (Storage) !== 'undefined') {
    console.log("Si esta disponible");
} else {
    console.log("No esta disponible");
}


// 1)Guardar datos en el local-storage (setItem)

localStorage.setItem("titulo", "Aprendiendo js con la ayuda de dios y luchando por una meta");

// 2) Recuperar elemento del localstorage y meterlo a mi pagia web
//1.accedemos al objeto localStorage 2.accedemos al metodo getitem
//Imprimimos en la consola
console.log(localStorage.getItem("titulo"));

// 3) Lo agregamos a un bloque de nuestro html
document.querySelector("#informacion").innerHTML = localStorage.getItem("titulo");


// 4) Guardar un objeto JSON en el localstorage
var usuario = {
    nombre: "Elio Alfonzo",
    profesion: "Desarrollador",
    web: "https://elioalfonzo.github.io/proyecto-html5-css3/"
}

// 5)Para guardar en nuetro local o enviar por http debemos convertirlo a un string

//lo podemos convertir con el metodo JSONstringify(usuario)
localStorage.setItem("usuario", JSON.stringify(usuario));

// 6) Recuperar Objeto (getItem) debemos convertir un objeto json en un objetode js usable

//con el metodo JSON.parse()
//localStorage.getItem('usuario') Asi lo recuperamos
var userjs = JSON.parse(localStorage.getItem('usuario')); //convertimos un objetousable por js
console.log(userjs);

document.querySelector("#datos").append(" Mi Pagina " + userjs.web + " - " + userjs.nombre);


//Podemos eliminar elementos del local storage
localStorage.clear();